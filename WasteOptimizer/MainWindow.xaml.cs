﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Mogotext.BusinessLogic.Data.DbContexts;
using Mogotext.BusinessLogic.Data.Repositories.impl;
using Mogotext.Shared.Entities;
using Mogotext.Shared.Entities.Enums;
using WasteOptimizer.Windows;

namespace WasteOptimizer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var conn = new MogotextDbContext();
            var repository =new  Repository<StepType>(conn);
          var t=  repository.GetById(1);
            MessageBox.Show(t.ToString());
        }

        private void PrintCharts(Grid grid)

        {

            PrintDialog print = new PrintDialog();

            if (print.ShowDialog() == true)
            {

                PrintCapabilities capabilities = print.PrintQueue.GetPrintCapabilities(print.PrintTicket);



                double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / grid.ActualWidth,

                                        capabilities.PageImageableArea.ExtentHeight / grid.ActualHeight);



                Transform oldTransform = grid.LayoutTransform;



                grid.LayoutTransform = new ScaleTransform(scale, scale);


                Size oldSize = new Size(grid.ActualWidth, grid.ActualHeight);

                Size sz = new Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

                grid.Measure(sz);
                ((UIElement)grid).Arrange(new Rect(new Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),

                    sz));



                print.PrintVisual(grid, "Print Results");

                grid.LayoutTransform = oldTransform;

                grid.Measure(oldSize);



                ((UIElement)grid).Arrange(new Rect(new Point(0, 0),

                    oldSize));

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
          //  PrintCharts(this.ReportPanel);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ViewDataWindow window = new ViewDataWindow();
            window.Show();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            CreateReport window = new CreateReport();
            window.Show();
        }
    }
}