﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WasteOptimizer.Windows
{
    /// <summary>
    /// Логика взаимодействия для ExecuteReport.xaml
    /// </summary>
    public partial class ExecuteReport : Window
    {
        public ExecuteReport()
        {
            InitializeComponent();
        }

        private void calendar1_DisplayModeChanged(object sender, CalendarModeChangedEventArgs e)
        {
            Calendar calendar = sender as Calendar;
            if (calendar.DisplayMode != CalendarMode.Year)
            {
                calendar.DisplayMode = CalendarMode.Year;
            }
        }

        private void calendar1_DisplayDateChanged(object sender, CalendarDateChangedEventArgs e)
        {
          //  MessageBox.Show(e.ToString());
        }
    }
}
