﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mogotext.Shared.Entities.Enums
{
 public   class StepType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
