﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mogotext.Shared.Entities
{
    class Order
    {
        public Article Article { get; set; }
        public float Length { get; set; }
        public DateTime ArriveDate { get; set; }
        public DateTime DepartedDate { get; set; }
    }
    class OrderState
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
