﻿using System.Collections.Generic;
using Mogotext.Shared.Entities.Enums;

namespace Mogotext.Shared.Entities
{
    public class ProcessMap
    {
        public int CurrentStep { get; set; }
        public LinkedList<OpeartionType> Process { get; set; }
    }
}