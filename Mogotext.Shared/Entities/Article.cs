﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mogotext.Shared.Entities
{
    class Article
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public ProcessMap ProcessMap { get; set; }

    }
}
