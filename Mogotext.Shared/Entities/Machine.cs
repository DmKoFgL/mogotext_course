﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mogotext.Shared.Entities.Enums;
using System.Data.SQLite;

namespace Mogotext.Shared.Entities
{
   
    public  class Machine
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public ICollection<MachineMode> Modes { get; set; }
    }
    public class MachineMode
    {
        public int Number { get; set; }
        public OpeartionType OpeartionType { get; set; }
    }

}
