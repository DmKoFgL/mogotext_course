﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mogotext.Shared.Entities;

namespace Mogotext.BusinessLogic.Services.Abstract
{
    interface IMachineService
    {
        IReadOnlyCollection<Machine> GetAll();
    }
}
