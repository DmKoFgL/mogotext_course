﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mogotext.BusinessLogic.Data.Repositories;
using Mogotext.BusinessLogic.Services.Abstract;
using Mogotext.Shared.Entities;

namespace Mogotext.BusinessLogic.Services.Impl
{
    class MachineService : IMachineService
    {
        private readonly IRepository<Machine> _machineRepository;
        public MachineService(IRepository<Machine> machineRepository)
        {
            _machineRepository = machineRepository;
        }
        public IReadOnlyCollection<Machine> GetAll()
        {
            return _machineRepository.GetAll().ToList();
        }
    }
}
