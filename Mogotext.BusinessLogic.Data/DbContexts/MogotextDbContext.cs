﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Mogotext.Shared.Entities;
using Mogotext.Shared.Entities.Enums;

namespace Mogotext.BusinessLogic.Data.DbContexts
{
   public class MogotextDbContext: DbContext
    {
        public MogotextDbContext():base("name=Mogotext")
        {
        }

      //  protected DbSet<Machine> Machines { get; set; }
        protected DbSet<StepType> StepTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<StepType>().ToTable("step_type");
        }
    }
}
