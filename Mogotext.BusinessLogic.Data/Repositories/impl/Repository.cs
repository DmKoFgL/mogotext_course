﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mogotext.BusinessLogic.Data.DbContexts;

namespace Mogotext.BusinessLogic.Data.Repositories.impl
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly MogotextDbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(MogotextDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsQueryable<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public bool Remove(int id)
        {
            var entity = this.GetById(id);
            return _dbSet.Remove(entity) != null;
        }

        public void Save(TEntity entity)
        {
            if (_dbSet.Contains(entity))
            {
                _dbSet.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                _dbSet.Add(entity);
            }
        }

        public void CommitChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
