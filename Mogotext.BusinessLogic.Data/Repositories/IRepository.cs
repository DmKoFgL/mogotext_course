﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mogotext.BusinessLogic.Data.Repositories
{
    public interface IRepository<TEntity>
           where TEntity : class
    {
        IQueryable<TEntity> GetAll();

        TEntity GetById(int id);

        bool Remove(int id);

        void Save(TEntity entity);

        void CommitChanges();
    }
}
